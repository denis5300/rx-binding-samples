package patterns.denis.com.rxsamples

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.BiFunction
import kotlinx.android.synthetic.main.activity_main.*
import java.util.concurrent.TimeUnit

class MainActivity : AppCompatActivity() {

    val compositeDisposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    @SuppressLint("CheckResult")
    override fun onStart() {
        super.onStart()

        compositeDisposable.addAll(
            //Карточка с bounce
            inputDebounce.asObservable()
                    .onMainThread()
                    .debounce(800, TimeUnit.MILLISECONDS)
                    .subscribe { outputDebounce.text = it.reversed() },
            //Карточка с merge
            Observable.merge(inputMerge1.asObservable(), inputMerge2.asObservable())
                    .onMainThread()
                    .subscribe {
                        if (it.isEmpty())
                            outputMerge.text = ""
                        else
                            outputMerge.text = "Last emitted string: $it"
                    },
            //Карточка с zip
            Observable.zip(inputZip1.asObservable(), inputZip2.asObservable(), BiFunction<String, String, String> { t1, t2 -> t1+ t2 })
                    .onMainThread()
                    .subscribe {
                        if (it.isEmpty())
                            outputMerge.text = ""
                        else
                            outputZip.text = "Zipped: $it"
                    },
            //Карточка с combine
            Observable.combineLatest(inputCombine1.asBehavior(), inputCombine2.asBehavior(), BiFunction<String, String, String> { t1, t2 -> t1+ t2 })
                    .onMainThread()
                    .subscribe {
                        if (it.isEmpty())
                            outputMerge.text = ""
                        else
                            outputCombine.text = "Combined: $it"
                    }
        )
    }

    override fun onStop() {
        super.onStop()

        compositeDisposable.clear()

    }
}
