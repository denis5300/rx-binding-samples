package patterns.denis.com.rxsamples

import android.annotation.SuppressLint
import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject

@SuppressLint("CheckResult")
fun <T> Observable<T>.onMainThread(): Observable<T> {
    subscribeOn(Schedulers.newThread())
    observeOn(AndroidSchedulers.mainThread())
    return this
}

fun EditText.asObservable(): PublishSubject<String> {

    val publishSubject = PublishSubject.create<String>()

    addTextChangedListener(object: TextWatcher {
        override fun afterTextChanged(p0: Editable?) {
            p0?.run {
                publishSubject.onNext(this.toString())
            }
        }
        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
    })

    return publishSubject
}

fun EditText.asBehavior(): BehaviorSubject<String> {

    val publishSubject = BehaviorSubject.createDefault(this.text.toString())

    addTextChangedListener(object: TextWatcher {
        override fun afterTextChanged(p0: Editable?) {
            p0?.run {
                publishSubject.onNext(this.toString())
            }
        }
        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
    })

    return publishSubject
}